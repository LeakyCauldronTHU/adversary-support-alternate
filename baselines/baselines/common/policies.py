import tensorflow as tf
from baselines.common import tf_util
from baselines.a2c.utils import fc
from baselines.common.distributions import make_pdtype
from baselines.common.input import observation_placeholder, encode_observation
from baselines.common.tf_util import adjust_shape
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.models import get_network_builder

import gym
from gym import spaces
import numpy as np


# def rebuild_policy(env, latent):
#     latent = tf.layers.flatten(latent)
#     # Based on the action space, will select what probability distribution type
#     pdtype = make_pdtype(env.action_space)
#     pd, _ = pdtype.pdfromlatent(latent, init_scale=0.01)
#
#     return pd


class PolicyWithValue(object):
    """
    Encapsulates fields and methods for RL policy and value function estimation with shared parameters
    """
    def __init__(self, env, observations, encoded_x, encoded_x_unattacked, observations_delta, bernoulli_probs,
                 prob_phi_sample, begin_retrain, latent, estimate_q=False,
                 vf_latent=None, critic_unattacked=None, policy_latent_unattacked=None, build_latent=None,
                 cross_entropy_loss_init=None, cross_entropy_loss_term=None, nbatch=None,
                 nsteps=None, attack_args=None, sess=None, **tensors):
        """
        Parameters:
        ----------
        env             RL environment

        observations    tensorflow placeholder in which the observations will be fed

        latent          latent state from which policy distribution parameters should be inferred

        vf_latent       latent state from which value function should be inferred (if None, then latent is used)

        sess            tensorflow session to run calculations in (if None, default session is used)

        **tensors       tensorflow tensors for additional attributes such as state or mask

        """
        self.attack_args = attack_args
        self.env = env
        self.X = observations
        self.encoded_x = encoded_x
        self.encoded_x_unattacked = encoded_x_unattacked
        self.observations_delta = observations_delta # delta added to X
        self.bernoulli_probs = bernoulli_probs
        self.prob_phi_sample = prob_phi_sample
        self.begin_retrain = begin_retrain
        self.build_latent = build_latent
        self.cross_entropy_loss_init = cross_entropy_loss_init
        self.cross_entropy_loss_term = cross_entropy_loss_term
        self.nbatch = nbatch
        self.nsteps = nsteps
        # print("inner check delta", self.X, self.X_delta)
        self.state = tf.constant([])
        self.initial_state = None
        self.estimate_q = estimate_q
        self.__dict__.update(tensors)
        # print("function check", self.cross_entropy_loss_init, self.cross_entropy_loss_term)
        # exit(0)

        vf_latent = vf_latent if vf_latent is not None else latent

        vf_latent = tf.layers.flatten(vf_latent)

        latent = tf.layers.flatten(latent)

        # Based on the action space, will select what probability distribution type
        self.pdtype = make_pdtype(env.action_space)

        # print("creating pd and pi ***********************************************************")
        self.pd, self.pi = self.pdtype.pdfromlatent(latent, init_scale=0.01)
        # the unattacked policy shares the same parameters with the attacked conterpart;
        self.pd_unattacked, _ = self.pdtype.pdfromlatent(policy_latent_unattacked, init_scale=0.01)
        self.policy_entropy_unattacked = tf.reduce_mean(self.pd_unattacked.entropy())
        self.critic_unattacked = critic_unattacked
        if attack_args.attack_type == 'critic':
            self.vf_unattacked = tf.reduce_sum(tf.exp(self.pd_unattacked.logdist()) * self.critic_unattacked, 1)
        else:
            self.vf_unattacked = tf.reduce_sum(self.critic_unattacked, 1)

        # print("pd and pi vreated ************************************************************")
        # print("inner check", tf.gradients(self.pd.entropy(), self.X), self.X)
        # exit(0)

        # Take an action
        self.action = self.pd.sample()
        self.A = self.pdtype.sample_placeholder([None])
        self.action_unattacked = self.pd_unattacked.sample()

        # Calculate the neg log of our probability
        self.neglogp = self.pd.neglogp(self.action)
        self.neglogp_unattacked = self.pd_unattacked.neglogp(self.action_unattacked)
        self.logdist = self.pd.logdist()
        self.critic_collapsed = tf.reduce_sum(tf.one_hot(self.A, env.action_space.n) * self.critic_unattacked, 1)

        self.sess = sess or tf.get_default_session()

        if estimate_q:
            # print("estimate_q in policies")
            # exit(0)
            assert isinstance(env.action_space, gym.spaces.Discrete)
            self.qf = fc(vf_latent, 'vf', env.action_space.n)
            self.qf_collapsed = tf.reduce_sum(self.qf * tf.one_hot(self.A, env.action_space.n), 1)
            # build value function based on action-value function
            self.vf = tf.reduce_sum(self.qf * tf.exp(self.logdist), 1)
        else:
            # print("creating value &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            self.vf = fc(vf_latent, 'vf', 1)
            # print("value created &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            self.vf = self.vf[:, 0]
            self.qf = False
            self.qf_collapsed = False

        self.results = self._build_test_attack()

    def _evaluate(self, variables, observation, begin_retrain=1.0, **extra_feed):
        sess = self.sess
        feed_dict = {self.X: adjust_shape(self.X, observation), self.begin_retrain: [begin_retrain], self.bernoulli_probs:[0.9]}

        for inpt_name, data in extra_feed.items():
            if inpt_name in self.__dict__.keys():
                inpt = self.__dict__[inpt_name]
                if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
                    feed_dict[inpt] = adjust_shape(inpt, data)

        # print("sess check", sess, variables)
        # exit(0)
        return sess.run(variables, feed_dict)

    def _build_test_attack(self):
        rand_start = self.attack_args.rand_start
        attack_step_size = self.attack_args.attack_step_size
        attack_steps = self.attack_args.attack_steps
        epsilon = self.attack_args.epsilon
        if rand_start:
            # print("shape check", np.shape(observation))
            noise = tf.random_normal(self.encoded_x.shape, 0, epsilon/4)
            noise = tf.clip_by_value(noise, -epsilon/2, epsilon/2)
            self.encoded_x = tf.clip_by_value(self.encoded_x+noise, 0, 255)

        logdist_static = tf.stop_gradient(tf.log(tf.exp(self.logdist) + 1e-5))
        cross_entropies = []
        logdists = []
        # cross_entropies.append(tf.exp(logdist_static))
        for h in range(attack_steps):
            policy_latent = self.build_latent(self.encoded_x, self.nbatch, self.nsteps)

            policy_dynamic, _ = self.pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)
            logdist_dynamic = policy_dynamic.logdist()
            # cross_entropies.append(((logdist_dynamic)))
            # cross_entropies.append(((logdist_static)))

            if self.attack_args.test_attack_type == 'policy':
                loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(logdist_dynamic)*logdist_static, 1))
            elif self.attack_args.test_attack_type == 'critic':
                if self.estimate_q:
                    loss = tf.reduce_sum(
                        -tf.reduce_sum(tf.exp(logdist_dynamic) * tf.stop_gradient(self.qf), 1))
                else:
                    loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(logdist_dynamic)*tf.stop_gradient(self.critic_unattacked), 1))
            elif self.attack_args.test_attack_type == 'critic_max':
                # print("currently not supported!")
                # exit(0)
                if self.estimate_q:
                    loss = tf.reduce_sum(
                        -tf.reduce_sum(tf.one_hot(tf.argmax(self.qf, 1), self.env.action_space.n) * logdist_dynamic, 1))
                else:
                    loss = tf.reduce_sum(
                        -tf.reduce_sum(tf.one_hot(tf.argmax(self.critic_unattacked, 1), self.env.action_space.n) * logdist_dynamic, 1))

            elif self.attack_args.test_attack_type == 'critic_min':
                # print("currently not supported!")
                # exit(0)
                if self.estimate_q:
                    loss = -tf.reduce_sum(
                        -tf.reduce_sum(tf.one_hot(tf.argmin(self.qf, 1), self.env.action_space.n) * logdist_dynamic, 1))
                else:
                    loss = -tf.reduce_sum(
                        -tf.reduce_sum(tf.one_hot(tf.argmin(self.critic_unattacked, 1), self.env.action_space.n) * logdist_dynamic, 1))
            else:
                loss = None
                print("unauthorized attack type when testing! exit!")
                exit(0)
            # cross_entropies.append(loss)
            cross_entropies.append(-tf.reduce_sum(tf.exp(logdist_dynamic)*logdist_static, 1)[0])
            noise = tf.sign(tf.gradients(loss, self.encoded_x)[0])
            # noise = (tf.gradients(loss, self.encoded_x)[0])

            # cross_entropies.append(tf.reduce_mean(noise))
            self.encoded_x = self.encoded_x + attack_step_size * noise
            self.encoded_x = tf.minimum(tf.maximum(self.encoded_x, self.encoded_x_unattacked - epsilon), self.encoded_x_unattacked + epsilon)
            self.encoded_x = (tf.clip_by_value(self.encoded_x, 0, 255))
        logdists.append(policy_dynamic.logdist())
        # cross_entropies.append(tf.exp(logdist_dynamic))
        return [self.encoded_x] + cross_entropies + logdists

    def build_test_attack(self, observation):
        results = self._evaluate(self.results, observation)
        # print("results in test attack", results)
        # exit(0)
        return results[0], results[1:]

    def step_unattacked(self, observation, **extra_feed):
        """
        This function will be used when train_attack is True; it has two utilities, one for training an unattacked
        Q-function for attack purpose; one for alternated training of attacked and unattacked policies
        :param observation:
        :param extra_feed:
        :return:
        """
        a, v, state, neglogp = \
            self._evaluate([self.action_unattacked, self.vf_unattacked, self.state,
                            self.neglogp_unattacked], observation, **extra_feed)
        # cro_ent_loss_init, cro_ent_loss_term, policy_entropy_unattacked = 0.0, 0.0, 0.0
        # print(a.astype(np.int))
        # exit(0)
        if state.size == 0:
            state = None

        return a, v, state, neglogp

    def step(self, observation, begin_retrain=1.0, **extra_feed):
        """
        Compute next action(s) given the observation(s)

        Parameters:
        ----------

        observation     observation data (either single or a batch)

        **extra_feed    additional data such as state or mask (names of the arguments should match the ones in constructor, see __init__)

        Returns:
        -------
        (action, value estimate, next state, negative log likelihood of the action under current policy parameters) tuple
        """
        # if used by runner, self.X_delta is determined by X, elif used by trainer, self.X_delta is determined by input
        if self.attack_args.train_attack:
            a, v, state, neglogp, obs_delta, cro_ent_loss_init, cro_ent_loss_term, policy_entropy_unattacked, begin, prob_sample = \
                self._evaluate([self.action, self.vf, self.state, self.neglogp, self.observations_delta,
                                self.cross_entropy_loss_init, self.cross_entropy_loss_term,
                                self.policy_entropy_unattacked, self.begin_retrain, self.prob_phi_sample],
                            observation, begin_retrain, **extra_feed)
            # print("begin retraining?", begin)
            # print("prob_sample", prob_sample)
        else:
            a, v, state, neglogp, obs_delta = \
                self._evaluate([self.action, self.vf, self.state, self.neglogp, self.observations_delta],
                               observation, **extra_feed)
            cro_ent_loss_init, cro_ent_loss_term, policy_entropy_unattacked = 0.0, 0.0, 0.0
        # print("state", state)
        # print("obs_delta", obs_delta)
        # exit(0)
        # print("action", a)
        # print("length of value", len(np.shape(v)), state.size)
        # exit(0)
        if len(np.shape(v))>1:
            a_temp = self.env.action_space.n
            index = np.eye(a_temp)[a]
            v = np.sum(v*index, 1)

        if state.size == 0:
            state = None
        return a, v, state, neglogp, obs_delta, cro_ent_loss_init, cro_ent_loss_term, policy_entropy_unattacked

    def value(self, ob, *args, **kwargs):
        """
        Compute value estimate(s) given the observation(s)

        Parameters:
        ----------

        observation     observation data (either single or a batch)

        **extra_feed    additional data such as state or mask (names of the arguments should match the ones in constructor, see __init__)

        Returns:
        -------
        value estimate
        """
        # values = self._evaluate(self.vf, ob, *args, **kwargs)
        # return values
        pass

    def save(self, save_path):
        tf_util.save_state(save_path, sess=self.sess)

    def load(self, load_path):
        tf_util.load_state(load_path, sess=self.sess)


def build_policy(env, policy_network, value_network=None,  normalize_observations=False, estimate_q=False,
                 attack_args=None, **policy_kwargs):
    # # if we use critic to calculate the attacked policy, make sure that Q is estimated
    # if attack_args.attack_type == 'critic':
    #     estimate_q = True

    # print("policy args", estimate_q, policy_kwargs, policy_network)
    # exit(0)
    if isinstance(policy_network, str):
        network_type = policy_network
        policy_network = get_network_builder(network_type)(**policy_kwargs)
        critic_network = get_network_builder(network_type)(**policy_kwargs)
        value_network = get_network_builder(network_type)(**policy_kwargs)

    print("policy_metwork:", policy_network, policy_kwargs)

    def build_latent(encoded_x, nbatch, nsteps):
        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent = policy_network(encoded_x)
            # print("verify policy_latent", policy_latent, isinstance(policy_latent, tuple))
            # exit(0)
            if isinstance(policy_latent, tuple):
                policy_latent, recurrent_tensors = policy_latent

                if recurrent_tensors is not None:
                    # recurrent architecture, need a few more steps
                    nenv = nbatch // nsteps
                    assert nenv > 0, 'Bad input for recurrent policy: batch size {} smaller than nsteps {}'.format(
                        nbatch, nsteps)
                    policy_latent, recurrent_tensors = policy_network(encoded_x, nenv)

            return policy_latent

    def build_latent_critic(encoded_x):
        with tf.variable_scope('critic', reuse=tf.AUTO_REUSE):
            vf_latent = critic_network(encoded_x)

        return vf_latent

    def pre_policy_process(encoded_x, ob_space, nbatch, nsteps, extra_tensors):
        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent = policy_network(encoded_x)

            assert not isinstance(policy_latent, tuple), 'we do not use recurrent policy in this project'
            if isinstance(policy_latent, tuple):
                policy_latent, recurrent_tensors = policy_latent

                if recurrent_tensors is not None:
                    # recurrent architecture, need a few more steps
                    nenv = nbatch // nsteps
                    assert nenv > 0, 'Bad input for recurrent policy: batch size {} smaller than nsteps {}'.format(
                        nbatch, nsteps)
                    policy_latent, recurrent_tensors = policy_network(encoded_x, nenv)
                    if extra_tensors:
                        extra_tensors.update(recurrent_tensors)

        with tf.variable_scope('vf', reuse=tf.AUTO_REUSE):
            # TODO recurrent architectures are not supported with value_network=copy yet
            vf_latent = value_network(encoded_x)

        return policy_latent, vf_latent, extra_tensors

    def policy_fn(nbatch=None, nsteps=None, sess=None, attack=None, observ_placeholder=None):
        '''
        :param nbatch:
        :param nsteps:
        :param sess:
        :param attack: is attack is True, then observation are attacked; if False, it means that the observation is
        already attacked
        :param observ_placeholder:
        '''
        ob_space = env.observation_space

        # to make sure that we only care about discrete action spaces
        assert isinstance(env.action_space, gym.spaces.Discrete)
        X = observ_placeholder if observ_placeholder is not None else observation_placeholder(ob_space, batch_size=nbatch)
        observation_delta = tf.placeholder(tf.float32, X.shape)
        begin_retrain = tf.placeholder(tf.float32, [1])
        bernoulli_probs = tf.placeholder(tf.float32, [1])

        extra_tensors = {}

        # observation normalization or not //// make sure that images are not normalized
        assert not normalize_observations
        if normalize_observations and X.dtype == tf.float32:
            encoded_x, rms = _normalize_clip_observation(X)
            extra_tensors['rms'] = rms
        else:
            encoded_x = X

        # data type transform
        encoded_x = encode_observation(ob_space, encoded_x)
        encoded_x_unattacked = encode_observation(ob_space, encoded_x)

        # build critic Q_\pi(s,a) and unattacked policy \pi(a|s)
        critic_latent = build_latent_critic(encoded_x)
        if attack_args.attack_type == 'critic':
            critic_unattacked = fc(critic_latent, 'critic', env.action_space.n)
        else:
            critic_unattacked = fc(critic_latent, 'critic', 1)
        policy_latent_unattacked, _, _ = pre_policy_process(encoded_x, ob_space, nbatch, nsteps, None)

        prob_phi = tf.distributions.Bernoulli(probs=bernoulli_probs)
        prob_phi_sample = tf.squeeze(prob_phi.sample(tf.shape(encoded_x)[0]), 1)

        # print("inner check parms", tf.trainable_variables('ppo2_model'))
        # exit(0)
        ################################################################################################################
        # attack when training #########################################################################################
        ################################################################################################################
        cross_entropy_loss_init = None
        cross_entropy_loss_term = None
        if not attack_args.train_attack:
            # calculate V_\pi(s) and \pi(a|s)
            policy_latent, vf_latent, _ = pre_policy_process(encoded_x, ob_space, nbatch, nsteps, extra_tensors)
            observation_delta = 0 * encoded_x
        elif attack_args.train_attack:
            if attack:
                # build policy that used by runner
                # print("attacked policy built")
                attack_step_size = attack_args.attack_step_size
                attack_steps = attack_args.attack_steps
                epsilon = attack_args.epsilon
                rand_start = attack_args.rand_start

                policy_latent = build_latent(encoded_x, nbatch, nsteps)
                pdtype = make_pdtype(env.action_space)

                policy, _ = pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)

                policy_static = tf.stop_gradient(tf.log(tf.exp(policy.logdist()) + 1e-5))

                if rand_start:
                    noise = tf.random_normal(tf.shape(encoded_x), 0, epsilon / 4)
                    noise = tf.clip_by_value(noise, -epsilon / 2, epsilon / 2)
                    encoded_x = (tf.clip_by_value(encoded_x + noise, 0, 255))

                for iter in range(attack_steps):
                    policy_latent = build_latent(encoded_x, nbatch, nsteps)
                    policy, _ = pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)
                    policy_dynamic = policy.logdist()
                    if attack_args.attack_type == 'critic':
                        loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(policy_dynamic) * tf.stop_gradient(critic_unattacked), 1))
                    else:
                        loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(policy_dynamic) * policy_static, 1))
                    if iter == 0:
                        cross_entropy_loss_init = tf.reduce_mean(-tf.reduce_sum(tf.exp(policy_dynamic) * policy_static, 1))
                    if iter == attack_steps - 1:
                        cross_entropy_loss_term = tf.reduce_mean(-tf.reduce_sum(tf.exp(policy_dynamic) * policy_static, 1))
                    gradients = tf.gradients(loss, encoded_x)
                    gradients = tf.sign(gradients[0])
                    encoded_x = encoded_x + gradients * attack_step_size
                    encoded_x = tf.minimum(tf.maximum(encoded_x, encoded_x_unattacked - epsilon), encoded_x_unattacked + epsilon)
                    encoded_x = (tf.clip_by_value(encoded_x, 0, 255))

                observation_delta = encoded_x - encoded_x_unattacked
                # calculate \pi(a|s+\delta)

                # calculate V_{\pi^\delta}(s)
                if attack_args.retrain:
                    policy_latent, _, extra_tensors = \
                        pre_policy_process(tf.where(tf.cast(begin_retrain, tf.bool)[0], encoded_x, encoded_x_unattacked),
                                           ob_space, nbatch, nsteps, extra_tensors)
                    _, vf_latent, _ = pre_policy_process(tf.where(tf.cast(begin_retrain, tf.bool)[0], encoded_x, encoded_x_unattacked),
                                                         ob_space, nbatch, nsteps, None)

                elif attack_args.rand_augment:
                    # always use the unattacked policy for retrain
                    policy_latent, _, extra_tensors = \
                        pre_policy_process(encoded_x_unattacked, ob_space, nbatch, nsteps, extra_tensors)

                    _, vf_latent, _ = pre_policy_process(encoded_x_unattacked, ob_space, nbatch, nsteps, None)
                else: # standard attack
                    print("standard attack")
                    policy_latent, _, extra_tensors = \
                        pre_policy_process(encoded_x, ob_space, nbatch, nsteps, extra_tensors)
                    _, vf_latent, _ = pre_policy_process(encoded_x_unattacked, ob_space, nbatch, nsteps, None)
            else:
                # construct policy for training
                # print("attacked policy build with delta placeholder")
                # calculate V_{\pi^\delta}(s)
                if attack_args.retrain:
                    _, vf_latent, _ = pre_policy_process(encoded_x+observation_delta * begin_retrain, ob_space, nbatch, nsteps, None)
                    policy_latent, _, _ = pre_policy_process(encoded_x + observation_delta * begin_retrain, ob_space,
                                                             nbatch, nsteps,
                                                             extra_tensors)
                elif attack_args.rand_augment:
                    # print("shape check", prob_phi.sample(tf.shape(encoded_x)[0]))
                    _, vf_latent, _ = pre_policy_process(
                        tf.where(tf.cast(prob_phi_sample, tf.bool), encoded_x+observation_delta, encoded_x), ob_space, nbatch, nsteps, None)
                    # calculate \pi(a|s+\delta)
                    policy_latent, _, _ = pre_policy_process(
                        tf.where(tf.cast(prob_phi_sample, tf.bool), encoded_x+observation_delta, encoded_x), ob_space, nbatch, nsteps,
                                                             extra_tensors)
                else:
                    print("standard attack")
                    _, vf_latent, _ = pre_policy_process(encoded_x, ob_space, nbatch, nsteps, None)
                    # calculate \pi(a|s+\delta)
                    policy_latent, _, _ = pre_policy_process(encoded_x+observation_delta, ob_space, nbatch, nsteps,
                                                                                 extra_tensors)

        policy = PolicyWithValue(
            env=env,
            observations=X,
            encoded_x=encoded_x,
            encoded_x_unattacked=encoded_x_unattacked,
            observations_delta=observation_delta,
            bernoulli_probs=bernoulli_probs,
            prob_phi_sample=prob_phi_sample,
            begin_retrain=begin_retrain,
            latent=policy_latent,
            vf_latent=vf_latent,
            critic_unattacked=critic_unattacked,
            policy_latent_unattacked=policy_latent_unattacked,
            build_latent=build_latent,
            cross_entropy_loss_init=cross_entropy_loss_init,
            cross_entropy_loss_term=cross_entropy_loss_term,
            nbatch=nbatch,
            nsteps=nsteps,
            attack_args=attack_args,
            sess=sess,
            estimate_q=estimate_q,
            **extra_tensors
        )

        # print("outer check gradient", tf.gradients(tf.reduce_mean(encoded_x), encoded_x))
        # exit(0)

        return policy

    return policy_fn


def _normalize_clip_observation(x, clip_range=[-5.0, 5.0]):
    rms = RunningMeanStd(shape=x.shape[1:])
    norm_x = tf.clip_by_value((x - rms.mean) / rms.std, min(clip_range), max(clip_range))
    return norm_x, rms

