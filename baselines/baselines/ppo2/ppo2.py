import os
import time
import numpy as np
import os.path as osp
from baselines import logger
from collections import deque
from baselines.common import explained_variance, set_global_seeds
from baselines.common.policies import build_policy
try:
    from mpi4py import MPI
except ImportError:
    MPI = None
from baselines.ppo2.runner import Runner


def constfn(val):
    def f(_):
        return val
    return f


def build_test_attack(policy):
    def attack(obs):
        noise = None
        return noise
    return attack


def learn(*, network, env, total_timesteps, eval_env = None, seed=None, nsteps=2048, ent_coef=0.0, lr=3e-4,
            vf_coef=0.5,  max_grad_norm=0.5, gamma=0.99, lam=0.95,
            log_interval=1, nminibatches=4, noptepochs=4, cliprange=0.2,
            save_interval=1000, load_path=None, model_fn=None, attack_args=None, **network_kwargs):
    '''
    Learn policy using PPO algorithm (https://arxiv.org/abs/1707.06347)

    Parameters:
    ----------

    network:                          policy network architecture. Either string (mlp, lstm, lnlstm, cnn_lstm, cnn, cnn_small, conv_only - see baselines.common/models.py for full list)
                                      specifying the standard network architecture, or a function that takes tensorflow tensor as input and returns
                                      tuple (output_tensor, extra_feed) where output tensor is the last network layer output, extra_feed is None for feed-forward
                                      neural nets, and extra_feed is a dictionary describing how to feed state into the network for recurrent neural nets.
                                      See common/models.py/lstm for more details on using recurrent nets in policies

    env: baselines.common.vec_env.VecEnv     environment. Needs to be vectorized for parallel environment simulation.
                                      The environments produced by gym.make can be wrapped using baselines.common.vec_env.DummyVecEnv class.


    nsteps: int                       number of steps of the vectorized environment per update (i.e. batch size is nsteps * nenv where
                                      nenv is number of environment copies simulated in parallel)

    total_timesteps: int              number of timesteps (i.e. number of actions taken in the environment)

    ent_coef: float                   policy entropy coefficient in the optimization objective

    lr: float or function             learning rate, constant or a schedule function [0,1] -> R+ where 1 is beginning of the
                                      training and 0 is the end of the training.

    vf_coef: float                    value function loss coefficient in the optimization objective

    max_grad_norm: float or None      gradient norm clipping coefficient

    gamma: float                      discounting factor

    lam: float                        advantage estimation discounting factor (lambda in the paper)

    log_interval: int                 number of timesteps between logging events

    nminibatches: int                 number of training minibatches per update. For recurrent policies,
                                      should be smaller or equal than number of environments run in parallel.

    noptepochs: int                   number of training epochs per update

    cliprange: float or function      clipping range, constant or schedule function [0,1] -> R+ where 1 is beginning of the training
                                      and 0 is the end of the training

    save_interval: int                number of timesteps between saving events

    load_path: str                    path to load the model from

    **network_kwargs:                 keyword arguments to the policy / network builder. See baselines.common/policies.py/build_policy and arguments to a particular type of network
                                      For instance, 'mlp' network architecture has arguments num_hidden and num_layers.



    '''

    # we set the save_interval to 2000 as default, i.e., save_interval=2000
    # print("save interval check", save_interval, network_kwargs)
    # exit(0)

    set_global_seeds(seed)

    # print("learning rate check", lr, isinstance(lr, float))
    # exit(0)
    if isinstance(lr, float): lr = constfn(lr)
    else: assert callable(lr)
    if isinstance(cliprange, float): cliprange = constfn(cliprange)
    else: assert callable(cliprange)
    total_timesteps = int(total_timesteps)
    pretrain_steps = int(attack_args.pretrain_steps)
    # print("policy network params", network_kwargs)
    # exit(0)

    policy = build_policy(env, network, attack_args=attack_args, **network_kwargs)

    # Get the nb of env
    nenvs = env.num_envs

    # Get state_space and action_space
    ob_space = env.observation_space
    ac_space = env.action_space

    # Calculate the batch_size
    nbatch = nenvs * nsteps
    nbatch_train = nbatch // nminibatches
    print("num check", nbatch, nsteps, nbatch_train)

    # Instantiate the model object (that creates act_model and train_model)
    # the model refers to the training model, not the network model; network model is policy
    if model_fn is None:
        from baselines.ppo2.model import Model
        model_fn = Model

    model = model_fn(policy=policy, ob_space=ob_space, ac_space=ac_space, nbatch_act=nenvs, nbatch_train=nbatch_train,
                     nsteps=nsteps, ent_coef=ent_coef, vf_coef=vf_coef,
                     max_grad_norm=max_grad_norm, attack_args=attack_args)

    if load_path is not None:
        model.load(load_path)
    # Instantiate the runner object
    runner = Runner(env=env, model=model, nsteps=nsteps, gamma=gamma, lam=lam)
    if eval_env is not None:
        eval_runner = Runner(env = eval_env, model = model, nsteps = nsteps, gamma = gamma, lam= lam)

    if attack_args.train_attack:
        epinfobuf = deque(maxlen=200)
        if eval_env is not None:
            eval_epinfobuf = deque(maxlen=200)
    else:
        epinfobuf = deque(maxlen=100)
        if eval_env is not None:
            eval_epinfobuf = deque(maxlen=100)

    # Start total timer
    tfirststart = time.time()
    previous_optimal_rewards = None
    update_best = False
    update_best_counter = 0

    nupdates = total_timesteps//nbatch
    nupdates_pretrain = pretrain_steps//nbatch
    alternate_interval = 1
    # if we use critic attack type, then we train \pi(a|s) and \pi(a|s+\delta) alternatively
    if attack_args.train_attack and (attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate'):
        alternate_interval = 2

    # print("check condition", MPI, save_interval, logger.get_dir(), (MPI is None or MPI.COMM_WORLD.Get_rank() == 0))
    # exit(0)
    for update in range(1, nupdates+1 + nupdates_pretrain*(attack_args.update_frac==0)):
        # print("is retraining", attack_args.retrain)
        if attack_args.retrain:
            if attack_args.update_frac == 0:
                if update > nupdates_pretrain:
                    begin_retrain = 1.0
                    update = update - nupdates_pretrain
                else:
                    begin_retrain = 0.0
            elif (update > nupdates // attack_args.update_frac) or (attack_args.update_frac >= 10):
                begin_retrain = 1.0
            else:
                begin_retrain = 0.0
        else:
            # if we donot use retrain, then set begin_retrain=1.0 for normal attack training
            begin_retrain = 1.0

        if attack_args.rand_augment:
            bernoulli_probs = attack_args.rand_frac / 10 * ((10 + 1) * update // nupdates)
        else:
            bernoulli_probs = 0.0

        assert nbatch % nminibatches == 0

        if begin_retrain:
            frac = 1.0 - (update - 1.0) / nupdates

        else:
            frac = 1.0 - (update - 1.0) / nupdates_pretrain

        if np.mod(update, alternate_interval) == 0:
            if attack_args.train_attack:
                print("ATTACKED PHASE")
            else:
                print("BASELINE TRAINING")
            # Start timer
            tstart = time.time()
            # annealing learning rate and clip range
            # Calculate the learning rate
            lrnow = lr(frac)
            # Calculate the cliprange
            cliprangenow = cliprange(frac)
            # Get minibatch (rollout, a trajectory)
            obs, obs_delta, returns, masks, actions, values, neglogpacs, cro_ent_loss_init, cro_ent_loss_term, \
            policy_entropy_unattacked, states, epinfos = runner.run(run_type='attacked',
                                                                    begin_retrain=begin_retrain)  # pylint: disable=E0632

            if eval_env is not None:
                eval_obs, eval_obs_delta, eval_returns, eval_masks, eval_actions, eval_values, eval_neglogpacs, \
                eval_states, eval_epinfos = eval_runner.run(mb_states='attacked',
                                                            begin_retrain=begin_retrain)  # pylint: disable=E0632

            epinfobuf.extend(epinfos)
            if eval_env is not None:
                eval_epinfobuf.extend(eval_epinfos)

            # Here what we're going to do is for each minibatch calculate the loss and append it.
            mblossvals = []
            # print("states", states)
            assert states is None, 'In this project we only support nonrecurrent version'
            if states is None:  # nonrecurrent version
                # Index of each element of batch_size
                # Create the indices array
                inds = np.arange(nbatch)
                # noptepochs ---> the each batch train noptepoch times
                # nbatch ---> nbatch = nenvs * nsteps, the total steps in each rollout
                # nbatch_train ---> nbatch // nminibatches, divide the mage batch into nbatch minibatchs
                # print("verify batch", noptepochs, nbatch, nbatch_train)
                # exit(0)
                for _ in range(noptepochs):
                    # Randomize the indexes
                    np.random.shuffle(inds)
                    # 0 to batch_size with batch_train_size step
                    for start in range(0, nbatch, nbatch_train):
                        end = start + nbatch_train
                        mbinds = inds[start:end]
                        slices = (arr[mbinds] for arr in (obs, obs_delta, returns, masks, actions, values, neglogpacs))
                        records = model.train(lrnow, cliprangenow, begin_retrain, bernoulli_probs, *slices)
                        mblossvals.append(records)
            else:  # recurrent version
                envsperbatch = nenvs // nminibatches
                envinds = np.arange(nenvs)
                flatinds = np.arange(nenvs * nsteps).reshape(nenvs, nsteps)
                for _ in range(noptepochs):
                    np.random.shuffle(envinds)
                    for start in range(0, nenvs, envsperbatch):
                        end = start + envsperbatch
                        mbenvinds = envinds[start:end]
                        mbflatinds = flatinds[mbenvinds].ravel()
                        slices = (arr[mbflatinds] for arr in
                                  (obs, obs_delta, returns, masks, actions, values, neglogpacs))
                        mbstates = states[mbenvinds]
                        mblossvals.append(
                            model.train(lrnow, cliprangenow, begin_retrain, bernoulli_probs, *slices, mbstates))
        else:
            print("UN-ATTACKED PHASE")
            # Start timer
            tstart = time.time()
            # annealing learning rate and clip range
            # Calculate the learning rate
            lrnow = lr(frac)
            # Calculate the cliprange
            cliprangenow = cliprange(frac)
            # Get minibatch (rollout, a trajectory)
            # print("begin running")
            obs, returns, masks, actions, values, neglogpacs, states, epinfos = runner.run(run_type='unattacked',
                                                                                           begin_retrain=begin_retrain)
            # print("running finished")
            if eval_env is not None:
                eval_obs, eval_returns, eval_masks, eval_actions, eval_values, eval_neglogpacs, \
                eval_states, eval_epinfos = eval_runner.run(run_type='unattacked',
                                                            begin_retrain=begin_retrain)  # pylint: disable=E0632

            epinfobuf.extend(epinfos)
            if eval_env is not None:
                eval_epinfobuf.extend(eval_epinfos)

            # Here what we're going to do is for each minibatch calculate the loss and append it.
            mblossvals = []
            assert states is None, 'In this project we only support nonrecurrent version'
            if states is None:  # nonrecurrent version
                # Index of each element of batch_size
                # Create the indices array
                inds = np.arange(nbatch)
                # noptepochs ---> the each batch train noptepoch times
                # nbatch ---> nbatch = nenvs * nsteps, the total steps in each rollout
                # nbatch_train ---> nbatch // nminibatches, divide the mage batch into nbatch minibatchs
                # print("verify batch", noptepochs, nbatch, nbatch_train)
                # exit(0)
                for _ in range(noptepochs):
                    # print("epoch", noptepochs)
                    # Randomize the indexes
                    np.random.shuffle(inds)
                    # 0 to batch_size with batch_train_size step
                    for start in range(0, nbatch, nbatch_train):
                        # print("start", start)
                        end = start + nbatch_train
                        mbinds = inds[start:end]
                        slices = (arr[mbinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                        records = model.train_unattacked(lrnow, cliprangenow, begin_retrain, bernoulli_probs, *slices)
                        mblossvals.append(records)
            else:  # recurrent version
                assert nenvs % nminibatches == 0
                envsperbatch = nenvs // nminibatches
                envinds = np.arange(nenvs)
                flatinds = np.arange(nenvs * nsteps).reshape(nenvs, nsteps)
                for _ in range(noptepochs):
                    np.random.shuffle(envinds)
                    for start in range(0, nenvs, envsperbatch):
                        end = start + envsperbatch
                        mbenvinds = envinds[start:end]
                        mbflatinds = flatinds[mbenvinds].ravel()
                        slices = (arr[mbflatinds] for arr in
                                  (obs, returns, masks, actions, values, neglogpacs))
                        mbstates = states[mbenvinds]
                        mblossvals.append(
                            model.train_unattacked(lrnow, cliprangenow, begin_retrain, bernoulli_probs, *slices,
                                                   mbstates))

        # Feedforward --> get losses --> update
        # print(mblossvals)
        # exit(0)
        lossvals = np.mean(mblossvals, axis=0)
        # End timer
        tnow = time.time()
        # Calculate the fps (frame per second)
        fps = int(nbatch / (tnow - tstart))
        if update % log_interval == 0 or update == 1:
            # Calculates if value function is a good predicator of the returns (ev > 1)
            # or if it's just worse than predicting nothing (ev =< 0)
            ev = explained_variance(values, returns)
            logger.logkv("serial_timesteps", update*nsteps)
            logger.logkv("nupdates", update)
            logger.logkv("total_timesteps", update*nbatch)
            logger.logkv("fps", fps)
            logger.logkv("explained_variance", float(ev))
            # print("check", type(epinfobuf), list(epinfobuf))
            # exit(0)
            if attack_args.retrain:
                logger.logkv('update_frac', begin_retrain)
                logger.logkv('frac_for_lr', frac)
            if attack_args.rand_augment:
                logger.logkv('bernoulli_probs', bernoulli_probs)
            if attack_args.train_attack:
                epinfobuf_tmp = list(epinfobuf)
                epinfobuf_tmp.reverse()
                epinfobuf_tmp = epinfobuf_tmp[::2]
                logger.logkv('eprewmean', safemean([epinfo['r'] for epinfo in epinfobuf_tmp]))
                logger.logkv('eplenmean', safemean([epinfo['l'] for epinfo in epinfobuf_tmp]))
            else:
                logger.logkv('eprewmean', safemean([epinfo['r'] for epinfo in epinfobuf]))
                logger.logkv('eplenmean', safemean([epinfo['l'] for epinfo in epinfobuf]))

            if attack_args.train_attack and np.mod(update, alternate_interval) == 0:
                logger.logkv('cross_entropy_loss_init', np.mean(cro_ent_loss_init))
                logger.logkv('cross_entropy_loss_term', np.mean(cro_ent_loss_term))
                logger.logkv('policy_entropy_unattacked', np.mean(policy_entropy_unattacked))
            if eval_env is not None:
                logger.logkv('eval_eprewmean', safemean([epinfo['r'] for epinfo in eval_epinfobuf]))
                logger.logkv('eval_eplenmean', safemean([epinfo['l'] for epinfo in eval_epinfobuf]))
            logger.logkv('time_elapsed', tnow - tfirststart)
            if attack_args.train_attack:
                for (lossval, lossname) in zip(lossvals, model.loss_names):
                    logger.logkv(lossname, lossval)
                # if np.mod(update, alternate_interval) == 0:
                #     for (lossval, lossname) in zip(lossvals, model.loss_names):
                #         logger.logkv(lossname, lossval)
                # else:
                #     # if use train_attack, the unattacked phase trains an unattacked critic, so we use loss_names_unattacked
                #     for (lossval, lossname) in zip(lossvals, model.loss_names_unattacked):
                #         logger.logkv(lossname, lossval)
            else:
                if np.mod(update, alternate_interval) == 0:
                    for (lossval, lossname) in zip(lossvals, model.loss_names):
                        logger.logkv(lossname, lossval)

            if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
                logger.dumpkvs()

        if save_interval and (update % save_interval == 0 or update == 1) and logger.get_dir() and (MPI is None or MPI.COMM_WORLD.Get_rank() == 0):
            checkdir = osp.join(logger.get_dir(), 'checkpoints')
            os.makedirs(checkdir, exist_ok=True)
            savepath = osp.join(checkdir, '%.5i'%update)
            print('Saving to', savepath)
            model.save(savepath)

        # # saving the best model so far
        # update_best_counter += 1
        # if previous_optimal_rewards is None or previous_optimal_rewards < safemean(
        #     [epinfo['l'] for epinfo in epinfobuf]):
        #     previous_optimal_rewards = safemean([epinfo['l'] for epinfo in epinfobuf])
        #     update_best = True
        # if update_best and update > 1 and update_best_counter >= 50 and logger.get_dir() and (
        #     MPI is None or MPI.COMM_WORLD.Get_rank() == 0):
        #     checkdir = osp.join(logger.get_dir(), 'checkpoints')
        #     os.makedirs(checkdir, exist_ok=True)
        #     savepath = osp.join(checkdir, 'best')
        #     print('Saving to', savepath)
        #     model.save(savepath)
        #     update_best = False
        #     update_best_counter = 0

    attack = model.build_test_attack
    # print("attack", attack)
    return model, attack
# Avoid division error when calculate the mean (in our case if epinfo is empty returns np.nan, not return an error)


def safemean(xs):
    return np.nan if len(xs) == 0 else np.mean(xs)



