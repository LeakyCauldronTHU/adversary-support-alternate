import tensorflow as tf
import functools

from baselines.common.tf_util import get_session, save_variables, load_variables
from baselines.common.tf_util import initialize

try:
    from baselines.common.mpi_adam_optimizer import MpiAdamOptimizer
    from mpi4py import MPI
    from baselines.common.mpi_util import sync_from_root
except ImportError:
    MPI = None


class Model(object):
    """
    We use this object to :
    __init__:
    - Creates the step_model
    - Creates the train_model

    train():
    - Make the training part (feedforward and retropropagation of gradients)

    save/load():
    - Save load the model
    """
    def __init__(self, *, policy, ob_space, ac_space, nbatch_act, nbatch_train,
                nsteps, ent_coef, vf_coef, max_grad_norm, attack_args, microbatch_size=None):
        assert attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy' or attack_args.attack_type \
               == 'policy_alternate', 'unsupported attack type'
        self.attack_args = attack_args
        # print("attack args check:", self.attack_args)
        self.sess = sess = get_session()

        with tf.variable_scope('ppo2_model', reuse=tf.AUTO_REUSE):
            # CREATE OUR TWO MODELS
            # act_model that is used for sampling
            # print("creating the first model")
            # if model is used for interacting with the environment, we need to generate perturbation
            act_model = policy(nbatch_act, 1, sess, True)

            # Train model for training
            # print("creating the second model")
            # if model is used for training, then we do not need to re-generate perturbation
            if microbatch_size is None:
                train_model = policy(nbatch_train, nsteps, sess, False)
            else:
                train_model = policy(microbatch_size, nsteps, sess, False)

            # print("nsteps", nbatch_train, microbatch_size, nsteps)
            # exit(0)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])  # returns; critics;
        # Keep track of old actor
        self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        # Cliprange
        self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

        neglogpac = train_model.pd.neglogp(A)

        # Calculate the entropy
        # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
        entropy = tf.reduce_mean(train_model.pd.entropy())
        if attack_args.train_attack:
            entropy_attacked = tf.reduce_mean(train_model.pd_unattacked.entropy())

        # CALCULATE THE LOSS
        # Total loss = Policy gradient loss - entropy * entropy coefficient + Value coefficient * value loss

        # Clip the value to reduce variability during Critic training
        # Get the predicted value, if we use estimate_q, then qf_collasped is used
        if isinstance(train_model.qf, bool):
            vpred = train_model.vf
        else:
            vpred = train_model.qf_collapsed
        # vpredclipped = OLDVPRED + tf.clip_by_value(train_model.vf - OLDVPRED, - CLIPRANGE, CLIPRANGE)
        vpredclipped = OLDVPRED + tf.clip_by_value(vpred - OLDVPRED, - CLIPRANGE, CLIPRANGE)
        # Unclipped value
        vf_losses1 = tf.square(vpred - R)
        # Clipped value
        vf_losses2 = tf.square(vpredclipped - R)
        vf_loss = tf.reduce_mean(tf.maximum(vf_losses1, vf_losses2))

        # code for training the critic function which is used for policy attack purpose
        if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
            # the collapsed critic Q_\pi(s,a)
            if attack_args.attack_type == 'critic':
                cpred = train_model.critic_collapsed
            else:
                cpred = train_model.vf_unattacked
            cpredclipped = OLDVPRED + tf.clip_by_value(cpred - OLDVPRED, - CLIPRANGE, CLIPRANGE)
            cf_losses1 = tf.square(cpred - R)
            cf_losses2 = tf.square(cpredclipped - R)
            cf_loss = tf.reduce_mean(tf.maximum(cf_losses1, cf_losses2))
            params_loss = sum([tf.nn.l2_loss(par) for par in tf.trainable_variables('ppo2_model/critic')])
            cf_loss += params_loss * self.attack_args.critic_regularization
            self.cf_loss = cf_loss

        # Calculate ratio (pi current policy / pi old policy)
        ratio = tf.exp(OLDNEGLOGPAC - neglogpac)

        # Defining Loss = - J is equivalent to max J
        # ADV is fully determined by input, input ADV can be determined by attacked or unattacked policy data
        pg_losses = -ADV * ratio

        pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)

        # Final PG loss
        pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
        approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
        clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

        # Total loss
        pi_loss = pg_loss - entropy * ent_coef

        # UPDATE THE PARAMETERS USING LOSS
        # 1. Get the model parameters
        params_pi = tf.trainable_variables('ppo2_model/pi')
        params_vf = tf.trainable_variables('ppo2_model/vf')
        # print("policy params check", params_pi)
        # print("value params check", params_vf)

        # 2. Build our trainer
        if MPI is not None:
            self.trainer_pi = MpiAdamOptimizer(MPI.COMM_WORLD, learning_rate=LR, epsilon=1e-5)
            self.trainer_vf = MpiAdamOptimizer(MPI.COMM_WORLD, learning_rate=LR, epsilon=1e-5)
            if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
                self.trainer_cf = MpiAdamOptimizer(MPI.COMM_WORLD, learning_rate=LR, epsilon=1e-5)
        else:
            self.trainer_pi = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            self.trainer_vf = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
                self.trainer_cf = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
        # 3. Calculate the gradients
        grads_and_var_pi = self.trainer_pi.compute_gradients(pi_loss, params_pi)
        grads_and_var_vf = self.trainer_vf.compute_gradients(vf_loss, params_vf)
        grads_pi, var_pi = zip(*grads_and_var_pi)
        grads_vf, var_vf = zip(*grads_and_var_vf)
        if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
            params_cf = tf.trainable_variables('ppo2_model/critic')
            grads_and_var_cf = self.trainer_cf.compute_gradients(cf_loss, params_cf)
            grads_cf, var_cf = zip(*grads_and_var_cf)

        if max_grad_norm is not None:
            # Clip the gradients (normalize)
            grads_pi, _grad_norm = tf.clip_by_global_norm(grads_pi, max_grad_norm)
            grads_vf, _grad_norm = tf.clip_by_global_norm(grads_vf, max_grad_norm)
            if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
                grads_cf, _grad_norm_critic = tf.clip_by_global_norm(grads_cf, max_grad_norm)

        grads_and_var_pi = list(zip(grads_pi, var_pi))
        grads_and_var_vf = list(zip(grads_vf, var_vf))
        if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
            grads_and_var_cf = list(zip(grads_cf, var_cf))

        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        # self.grads_pi = grads_pi
        # self.grads_vf = grads_vf
        # self.var_pi = var_pi
        # self.var_vf = var_vf
        self._train_op = tf.group(self.trainer_pi.apply_gradients(grads_and_var_pi),
                                  self.trainer_vf.apply_gradients(grads_and_var_vf))

        if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
            self._train_op_unattacked = tf.group(self.trainer_pi.apply_gradients(grads_and_var_pi),
                                                 self.trainer_cf.apply_gradients(grads_and_var_cf))

        if isinstance(train_model.qf, bool):
            self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
        else:
            self.loss_names = ['policy_loss', 'critic_loss', 'policy_entropy', 'approxkl', 'clipfrac']
        self.stats_list = [pg_loss, vf_loss, entropy, approxkl, clipfrac]
        if attack_args.train_attack:
            self.loss_names += ['entropy_attacked']

        if attack_args.attack_type == 'critic' or attack_args.attack_type == 'policy_alternate':
            if attack_args.attack_type == 'critic':
                self.loss_names_unattacked = ['policy_loss', 'critic_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            else:
                self.loss_names_unattacked = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list_unattacked = [pg_loss, cf_loss, entropy, approxkl, clipfrac]

        self.train_model = train_model
        self.act_model = act_model
        # print("model check", self.act_model, self.train_model)
        self.step = act_model.step # step forward for one step
        self.step_unattacked = act_model.step_unattacked
        self.value = act_model.value
        self.initial_state = act_model.initial_state
        self.build_test_attack = act_model.build_test_attack

        self.save = functools.partial(save_variables, sess=sess)
        self.load = functools.partial(load_variables, sess=sess)

        initialize()
        global_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="")
        if MPI is not None:
            sync_from_root(sess, global_variables) #pylint: disable=E1101

    def train(self, lr, cliprange, begin_retrain, bernoulli_probs, obs, obs_delta, returns, masks, actions, values, neglogpacs, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # print("begin retrain in train", begin_retrain)

        # # if we use action-value estimation, we may use critics-values as advantages
        # adv = critics - values

        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)

        td_map = {
            self.train_model.X : obs,
            self.train_model.A: actions,
            self.train_model.observations_delta : obs_delta,
            self.train_model.begin_retrain: [begin_retrain],
            self.train_model.bernoulli_probs: [bernoulli_probs],
            self.A : actions,
            self.ADV : advs,
            self.R : returns,
            self.LR : lr,
            self.CLIPRANGE : cliprange,
            self.OLDNEGLOGPAC : neglogpacs,
            self.OLDVPRED : values
        }
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks

        # print("attacked training")
        return self.sess.run(
            self.stats_list + [self._train_op],
            td_map
        )[:-1]

    def train_unattacked(self, lr, cliprange, begin_retrain, bernoulli_probs, obs, returns, masks, actions, values, neglogpacs, states=None):
        advs = returns - values

        # print('begin retrain in unattacked train', begin_retrain)
        # # if we use action-value estimation, we may use critics-values as advantages
        # adv = critics - values

        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)

        td_map = {
            self.train_model.X: obs,
            self.train_model.observations_delta: 0*obs,
            self.train_model.begin_retrain: [begin_retrain],
            self.train_model.bernoulli_probs: [bernoulli_probs],
            self.train_model.A: actions,
            self.A: actions,
            self.ADV: advs,
            self.R: returns,
            self.LR: lr,
            self.CLIPRANGE: cliprange,
            self.OLDNEGLOGPAC: neglogpacs,
            self.OLDVPRED: values
        }
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks

        return self.sess.run(self.stats_list_unattacked + [self._train_op_unattacked], td_map)[:-1]

