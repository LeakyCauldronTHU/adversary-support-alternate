import sys
import multiprocessing
import os.path as osp
import gym
import subprocess
import os
from collections import defaultdict
import tensorflow as tf
import numpy as np
import datetime

from baselines.common.vec_env import VecFrameStack, VecNormalize, VecEnv
from baselines.common.vec_env.vec_video_recorder import VecVideoRecorder
from baselines.common.cmd_util import common_arg_parser, parse_unknown_args, make_vec_env, make_env, attack_parser
from baselines.common.tf_util import get_session
from baselines import logger
from importlib import import_module
import cv2
import time

import matplotlib.pyplot as plt

try:
    from mpi4py import MPI
except ImportError:
    MPI = None

try:
    import pybullet_envs
except ImportError:
    pybullet_envs = None

try:
    import roboschool
except ImportError:
    roboschool = None

_game_envs = defaultdict(set)
for env in gym.envs.registry.all():
    # TODO: solve this with regexes
    env_type = env._entry_point.split(':')[0].split('.')[-1]
    _game_envs[env_type].add(env.id)

# reading benchmark names directly from retro requires
# importing retro here, and for some reason that crashes tensorflow
# in ubuntu
_game_envs['retro'] = {
    'BubbleBobble-Nes',
    'SuperMarioBros-Nes',
    'TwinBee3PokoPokoDaimaou-Nes',
    'SpaceHarrier-Nes',
    'SonicTheHedgehog-Genesis',
    'Vectorman-Genesis',
    'FinalFight-Snes',
    'SpaceInvaders-Snes',
}


def train(args, attack_args, extra_args):
    env_type, env_id = get_env_type(args)
    print('env_type: {}'.format(env_type))

    total_timesteps = int(args.num_timesteps)
    seed = args.seed

    learn = get_learn_function(args.alg)  # get deepq.learn
    alg_kwargs = get_learn_function_defaults(args.alg, env_type)
    # print("default alg_kwargs", alg_kwargs)
    alg_kwargs.update(extra_args)

    env = build_env(args)
    if args.save_video_interval != 0:
        env = VecVideoRecorder(env, osp.join(logger.get_dir(), "videos"), record_video_trigger=lambda x: x % args.save_video_interval == 0, video_length=args.save_video_length)

    if args.network:
        alg_kwargs['network'] = args.network
    else:
        if alg_kwargs.get('network') is None:
            alg_kwargs['network'] = get_default_network(env_type)

    print('Training {} on {}:{} with arguments \n{}'.format(args.alg, env_type, env_id, alg_kwargs))
    # print("args", attack_args, alg_kwargs)
    # exit(0)
    # print("call learning function check", alg_kwargs)
    model, attack = learn(
        env=env,
        seed=seed,
        total_timesteps=total_timesteps,
        attack_args=attack_args,
        **alg_kwargs
    )

    # print("attack info", attack)
    return model, attack, env


def build_env(args):
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2
    nenv = args.num_env or ncpu
    alg = args.alg
    seed = args.seed
    max_episode_steps = args.max_episode_steps

    env_type, env_id = get_env_type(args)

    if env_type in {'atari', 'retro'}:
        if alg == 'deepq':
            env = make_env(env_id, env_type, seed=seed, wrapper_kwargs={'frame_stack': True})
        elif alg == 'trpo_mpi':
            env = make_env(env_id, env_type, seed=seed)
        else:
            frame_stack_size = 4
            env = make_vec_env(env_id, env_type, nenv, seed, gamestate=args.gamestate, reward_scale=args.reward_scale, max_episode_steps=max_episode_steps)
            env = VecFrameStack(env, frame_stack_size)

    else:
        config = tf.ConfigProto(allow_soft_placement=True,
                               intra_op_parallelism_threads=1,
                               inter_op_parallelism_threads=1)
        config.gpu_options.allow_growth = True
        get_session(config=config)

        flatten_dict_observations = alg not in {'her'}
        env = make_vec_env(env_id, env_type, args.num_env or 1, seed, reward_scale=args.reward_scale, flatten_dict_observations=flatten_dict_observations)

        if env_type == 'mujoco':
            env = VecNormalize(env)

    return env


def get_env_type(args):
    env_id = args.env

    if args.env_type is not None:
        return args.env_type, env_id

    # Re-parse the gym registry, since we could have new envs since last time.
    for env in gym.envs.registry.all():
        env_type = env._entry_point.split(':')[0].split('.')[-1]
        _game_envs[env_type].add(env.id)  # This is a set so add is idempotent

    if env_id in _game_envs.keys():
        env_type = env_id
        env_id = [g for g in _game_envs[env_type]][0]
    else:
        env_type = None
        for g, e in _game_envs.items():
            if env_id in e:
                env_type = g
                break
        assert env_type is not None, 'env_id {} is not recognized in env types'.format(env_id, _game_envs.keys())

    return env_type, env_id


def get_default_network(env_type):
    if env_type in {'atari', 'retro'}:
        return 'cnn'
    else:
        return 'mlp'


def get_alg_module(alg, submodule=None):
    submodule = submodule or alg
    # print("path check", '.'.join(['baselines', alg, submodule]))
    # exit(0)
    try:
        # first try to import the alg module from baselines
        alg_module = import_module('.'.join(['baselines', alg, submodule]))
    except ImportError:
        # then from rl_algs
        alg_module = import_module('.'.join(['rl_' + 'algs', alg, submodule]))

    return alg_module


def get_learn_function(alg):
    return get_alg_module(alg).learn


def get_attack_function(alg):
    return get_alg_module(alg).attack


def get_learn_function_defaults(alg, env_type):
    try:
        alg_defaults = get_alg_module(alg, 'defaults')
        kwargs = getattr(alg_defaults, env_type)()
    except (ImportError, AttributeError):
        kwargs = {}
    return kwargs


def parse_cmdline_kwargs(args):
    '''
    convert a list of '='-spaced command-line arguments to a dictionary, evaluating python objects when possible
    '''
    def parse(v):

        assert isinstance(v, str)
        try:
            return eval(v)
        except (NameError, SyntaxError):
            return v

    return {k: parse(v) for k,v in parse_unknown_args(args).items()}

#
# def multi_step_attack(attack, obs, attack_args):
#     epsilon = attack_args.epsilon
#     step_size = attack_args.attack_step_size
#     steps = attack_args.attack_steps
#     obs_unattacked = np.copy(obs)
#
#     # use random start in L_inf ball during test
#     if attack_args.rand_start:
#         noise = np.random.normal(0, epsilon/4, np.shape(obs))
#         noise = np.clip(noise, -epsilon/2, epsilon/2)
#         obs = np.clip(obs + noise, 0, 255)
#
#     for h in range(steps):
#         noise, cross_entropy = attack(np.copy(obs), np.copy(obs_unattacked), True)
#         print("cross entropy", cross_entropy)
#         obs = obs + step_size * np.sign(noise)
#         obs = np.minimum(np.maximum(obs, obs_unattacked - epsilon), obs_unattacked + epsilon)
#         obs = np.clip(obs, 0, 255)
#
#     return obs, obs - obs_unattacked


def multi_step_attack(attack, obs):
    obs_attacked, elsewise = attack(obs)
    cross_entropies, logdists = elsewise[0:-1], elsewise[-1]
    print("test attack check", cross_entropies)

    return obs_attacked, obs_attacked - obs, np.array(logdists), np.array(cross_entropies)


def compute_kl_divergence(logdists, name=None):
    logdists = [logdists[:, i, :] for i in range(np.shape(logdists)[1])]
    logdists = np.array(logdists)
    kl_divergence = np.zeros([np.shape(logdists)[0], np.shape(logdists)[1], np.shape(logdists)[1]])
    for k in range(np.shape(logdists)[0]):
        for i in range(np.shape(logdists)[1]):
            for j in range(np.shape(logdists)[1]):
                kl_divergence[k, i, j] = np.sum(np.exp(logdists[k, i, :]) * (logdists[k, i, :] - logdists[k, j, :]))

    # print("cross entropy", kl_divergence)

    print('kl divergence check', np.shape(kl_divergence))
    # exit(0)
    plt.figure()
    L = np.shape(kl_divergence)[0]
    if L > 1:
        for i in range(L):
            plt.subplot(2, L // 2, i+1)
            plt.imshow(kl_divergence[i, :, :])
    else:
        plt.imshow(kl_divergence[0, :, :])
        plt.title(name)
        plt.colorbar()
    plt.show()
    plt.close()
    # return kl_divergence


def plot_attack_curves(cross_entropies, name=None):
    # print("cross entropies check", np.shape(cross_entropies))
    # exit(0)
    plt.figure()
    index = [i for i in range(np.shape(cross_entropies)[1])]
    for i in range(np.shape(cross_entropies)[0]):
        plt.plot(index, cross_entropies[i,:], linewidth=1.0)
    plt.title(name)
    plt.xlabel('attack steps')
    plt.ylabel('cross entropies')
    plt.show()
    plt.close()


def str_process(strings):
    components = strings.split('_')
    short = ''
    for i in range(len(components)):
        short += components[i][0:3]
        short += '_'
    short = short[:-1]
    return short

def env_process(env):
    if isinstance(env, str):
        if 'NoFrameskip' in env:
            env = env.split('NoFrameskip')[0]

    return env


def delete_old_logs(dir, logs):
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    try:
        print("is trying dir", dir)
        existing_logs = os.listdir(dir)
        print("existing jobs", existing_logs)

        for lg in existing_logs:
            if logs in lg:
                print("we have found old logs", lg, logs)
                cmd = 'rm -rf ' + os.path.join(dir, lg)
                print(cmd)
                subprocess.call(cmd, shell=True)

    except:
        print("there are no old files")
        pass
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")


def main(args):
    args_sys = args
    arg_parser = common_arg_parser()
    args, unknown_args = arg_parser.parse_known_args(args_sys)
    extra_args_1 = parse_cmdline_kwargs(unknown_args)

    if args.cluster is not None:
        args.num_env = 16
        args.log_path = '//philly/' + args.cluster + '/resrchvc/v-wancha/logs_final/'

    if args.play:
        args.num_env = 1
        args.save_video_interval = 1
        args.save_video_length = 10000
    # print("args", args.cluster, args.num_env, args.log_path)
    # print("available extra args: estimate_q (default: False), save_interval (default:1000)")

    ##########################################################
    ##########################################################
    attack_args_parser = attack_parser()
    attack_args, unknown_args = attack_args_parser.parse_known_args(args_sys)
    extra_args_2 = parse_cmdline_kwargs(unknown_args)
    # assert attack_args.retrain, 'this code only support retrain'
    # assert attack_args.attack_type=='policy', 'this code only support policy attack'
    # print("attack_args", attack_args)

    extra_args = dict.fromkeys([x for x in extra_args_1 if x in extra_args_2])
    for t in extra_args.keys():
        extra_args[t] = extra_args_1[t]
    # print("extra_args", extra_args)

    # include args info into log directory
    args_dict, attack_args_dict = args.__dict__, attack_args.__dict__
    infos_args = ['env', 'seed']
    # infos_args = ['env', 'seed']
    if attack_args.train_attack:
        if attack_args.retrain:
            infos_attack_args = ['train_attack', 'attack_type', 'epsilon', 'retrain', 'update_frac']
        elif attack_args.rand_augment:
            infos_attack_args = ['train_attack', 'attack_type', 'epsilon', 'rand_augment', 'rand_frac']
        else:
            infos_attack_args = ['train_attack', 'attack_type', 'epsilon', 'retrain'] # 'attack_steps', 'attack_step_size', estimate_q, 'critic_regularization'

        infos_extra_args = ['network_multiply']
    else:
        infos_attack_args = ['train_attack']
        infos_extra_args = ['network_multiply', 'estimate_q']

    if attack_args.rand_augment or attack_args.retrain:
        assert attack_args.train_attack, 'must be in the training attack phase'
        assert attack_args.attack_type == 'policy', 'currently we only support policy attack'

    extra_args_log = {}
    for i in range(len(infos_extra_args)):
        if infos_extra_args[i] not in extra_args.keys():
            extra_args_log.update({infos_extra_args[i]: None})
    extra_args_log.update(extra_args)

    dir_tmp = ''
    if args.log_path is None:
        args.log_path = '~'

    for i in range(len(infos_args)):
        dir_tmp = dir_tmp + str_process(infos_args[i]) + '=' + str(env_process(args_dict[infos_args[i]])) + '-'
    for i in range(len(infos_attack_args)):
        dir_tmp = dir_tmp + str_process(infos_attack_args[i]) + '=' + str(attack_args_dict[infos_attack_args[i]]) + '-'
    for i in range(len(infos_extra_args)):
        dir_tmp = dir_tmp + str_process(infos_extra_args[i]) + '=' + str(extra_args_log[infos_extra_args[i]]) + '-'

    delete_old_logs(args.log_path, dir_tmp)

    dir_tmp = dir_tmp + datetime.datetime.now().strftime("%m_%d_%H_%M_%S_%f") + '_last'

    args.log_path = osp.join(args.log_path, dir_tmp)
    if args.log_path[0] == '~':
        args.log_path = osp.expanduser(args.log_path)
    # print("log path", args.log_path)
    # print("num_envs", args.num_env)
    # exit(0)

    if args.extra_import is not None:
        import_module(args.extra_import)

    if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
        rank = 0
        logger.configure(dir=args.log_path)
    else:
        logger.configure(format_strs=[])
        rank = MPI.COMM_WORLD.Get_rank()

    # loggng hyper-parameter configurations args, attack_args and extra args
    logger.log("*******************standard args**********************")
    for k in args_dict.keys():
        logger.log(k, args_dict[k])
    logger.log("*******************attack args************************")
    for k in attack_args_dict.keys():
        logger.log(k, attack_args_dict[k])
    logger.log("*******************extra args*************************")
    for k in extra_args.keys():
        logger.log(k, extra_args[k])
    logger.log("******************************************************")

    model, attack, env = train(args, attack_args, extra_args)

    # save the final trained model
    save_path = osp.join(args.log_path, 'checkpoints', 'last')
    # print("save path", save_path)
    # if args.save_path is not None and rank == 0:
    if rank == 0:
        # save_path = osp.expanduser(args.save_path)
        model.save(save_path)

    if args.play:
        rewards = []
        counter = 0
        step = 0
        show_freq = 1000000
        random_starts = 50
        logger.log("Running trained model")
        obs = env.reset()

        state = model.initial_state if hasattr(model, 'initial_state') else None
        dones = np.zeros((1,))

        episode_rew = 0
        while True:
            step += 1
            # print("step", step)
            if attack_args.test_attack:
                if np.mod(step, show_freq) == 0:
                    logdists = []
                    cross_entropies = []
                    for _ in range(random_starts):
                        _, _, logdist, cross_entropy = multi_step_attack(attack, np.expand_dims(obs, 0) if len(np.shape(obs))==3 else obs)
                        logdists.append(logdist)
                        cross_entropies.append(cross_entropy)

                    plot_attack_curves(np.array(cross_entropies), name='cross entropy loss curves with ' + str(random_starts) + ' different random starts')
                    compute_kl_divergence(np.array(logdists), name='kl-divergences distribution with\n' + str(random_starts) + ' different random starts')

                obs_attacked, noise, _, _ = multi_step_attack(attack,
                                                          np.expand_dims(obs, 0) if len(np.shape(obs)) == 3 else obs)

                plt.figure()
                plt.subplot(1,3,1)
                plt.imshow(obs[:, :, 0] if len(np.shape(obs)) == 3 else obs[-1, :, :, 0])
                plt.title('original frame')
                plt.subplot(1,3,2)
                a = plt.imshow(noise[:, :, 0] if len(np.shape(noise))==3 else noise[-1,:,:,0])
                # plt.colorbar(shrink=.8)
                plt.title('adversarial attack')
                obs = obs_attacked
                plt.subplot(1,3,3)
                plt.imshow(obs[:, :, 0] if len(np.shape(obs))==3 else obs[-1,:,:,0])
                plt.title('attacked frame')

                plt.suptitle('attack type=' + attack_args.test_attack_type + ' ' + 'attack steps=' + str(attack_args.attack_steps))

                if np.mod(step, show_freq) == 0:
                    plt.show()
                plt.close()

            if state is not None:
                actions, _, state, _, _, _, _, _ = model.step(obs, S=state, M=dones)
                # actions, _, state, _ = model.step(obs, S=state, M=dones)
            else:
                actions, _, _, _, _, _, _, _ = model.step(obs)
                # actions, _, _, _ = model.step(obs)
            obs, rew, done, info = env.step(actions)
            # print(info)
            if 'episode' in info[0]:
                print(step, info)
            episode_rew += rew[0] if isinstance(env, VecEnv) else rew
            env.render()

            done = done.any() if isinstance(done, np.ndarray) else done
            if done:
                print(f'episode={counter+1}, episode_rew={episode_rew}')
                rewards.append(episode_rew)
                episode_rew = 0
                print("will reset the environment _________+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                obs = env.reset()
                counter += 1

                if counter == 100:
                    if counter == 50:
                        print("mean rewards in 100 episodes:", sum(rewards)/len(rewards))
                    if counter == 20:
                        print("mean rewards in 100 episodes:", sum(rewards)/len(rewards))
                    print("mean rewards in 100 episodes:", sum(rewards) / len(rewards))
                    plt.plot(range(len(rewards)), rewards)
                    # plt.show()
                    break

    env.close()

    return model


if __name__ == '__main__':
    main(sys.argv)
